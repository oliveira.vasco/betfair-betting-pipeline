import json
from typing import Dict, List

from airflow.decorators import task

from betfairlightweight.resources import MarketBook

from betfair_broker_pipeline.source.bet_calculator.calculator import run_bet_calculator
from betfair_broker_pipeline.source.utils.db_utils import insert_betting_record
from betfair_broker_pipeline.source.utils.s3_utils import download_dict_from_s3, delete_keys_from_s3


@task
def calculate_bets(market_to_s3: Dict[str, str]) -> List[int]:
    record_ids = []
    for market_id, s3_key in market_to_s3.items():
        market_book = MarketBook(**json.loads(download_dict_from_s3(s3_key)))
        bets = run_bet_calculator(market_book)
        for bet in bets:
            record_ids.append(insert_betting_record(**bet))
    delete_keys_from_s3(list(market_to_s3.values()))
    return record_ids
