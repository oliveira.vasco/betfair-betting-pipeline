from airflow.decorators import task_group

from betfair_broker_pipeline.source.tasks.liveliness_task import check_liveliness
from betfair_broker_pipeline.source.tasks.list_markets_task import list_markets
from betfair_broker_pipeline.source.tasks.fetch_market_data_task import fetch_market_data
from betfair_broker_pipeline.source.tasks.select_markets_task import select_markets
from betfair_broker_pipeline.source.tasks.calculate_bets_task import calculate_bets
from betfair_broker_pipeline.source.tasks.run_bets_task import run_bets


@task_group(group_id="betting_task_group")
def betting_task_group():
    liveliness_check = check_liveliness()
    run_bets(calculate_bets(fetch_market_data(select_markets(list_markets(liveliness_check)))))
    return liveliness_check
