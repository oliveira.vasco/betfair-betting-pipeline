from datetime import datetime
from typing import List

from airflow.decorators import task
from betfairlightweight.filters import market_filter, time_range

from betfair_broker_pipeline.source.utils.betfairlightweight_utils import get_logged_client
from betfair_broker_pipeline.source.config import (
    MARKET_FILTER_LIST_OF_EVENT_TYPE_IDS,
    MARKET_FILTER_LIST_OF_COUNTRIES,
    MARKET_FILTER_TIME_DELTA_FROM_NOW,
    MARKET_FILTER_TYPE_CODES,
    MARKET_CATALOGUE_MAX_RESULTS,
)


@task
def list_markets(_: None) -> List[str]:
    betfair_client = get_logged_client()
    query_filter = market_filter(
        event_type_ids=MARKET_FILTER_LIST_OF_EVENT_TYPE_IDS,
        market_countries=MARKET_FILTER_LIST_OF_COUNTRIES,
        market_type_codes=MARKET_FILTER_TYPE_CODES,
        market_start_time=_get_range_from_time_delta(),
    )
    markets = betfair_client.betting.list_market_catalogue(query_filter, max_results=MARKET_CATALOGUE_MAX_RESULTS)
    return list(map(lambda m: m.market_id, markets))


def _get_range_from_time_delta() -> dict:
    utc_now = datetime.utcnow()
    return time_range(utc_now, utc_now + MARKET_FILTER_TIME_DELTA_FROM_NOW)
