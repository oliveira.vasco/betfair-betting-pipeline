from typing import Dict, List

from betfairlightweight.filters import price_projection, price_data

from airflow.decorators import task

from betfair_broker_pipeline.source.utils.betfairlightweight_utils import get_logged_client
from betfair_broker_pipeline.source.utils.s3_utils import upload_dict_to_s3


@task
def fetch_market_data(market_ids: List[str]) -> Dict[str, str]:
    betfair_client = get_logged_client()
    market_books = betfair_client.betting.list_market_book(
        market_ids=market_ids,
        price_projection=price_projection(price_data(ex_all_offers=True)),
    )
    market_to_s3: Dict[str, str] = {}
    for book in market_books:
        market_to_s3[book.market_id] = upload_dict_to_s3(f"market-book-{book.market_id}", book.json())
    return market_to_s3
