from typing import List

from airflow.decorators import task

from betfair_broker_pipeline.source.utils.db_utils import query_markets_in_betting_record


@task
def select_markets(market_ids: List[str]) -> List[str]:
    markets_recorded = query_markets_in_betting_record(market_ids)
    return list(set(market_ids) - markets_recorded)
