from decimal import Decimal
from typing import List

from airflow.decorators import task

from betfairlightweight.filters import place_instruction
from betfairlightweight.resources import PlaceOrders

from betfair_broker_pipeline.source.utils.betfairlightweight_utils import place_order
from betfair_broker_pipeline.source.constants import RESPONSE_SUCCESS, BettingRecordStatus
from betfair_broker_pipeline.source.models.bet_record import BetRecord
from betfair_broker_pipeline.source.utils.db_utils import query_markets_by_id_in_betting_record, update_betting_record_field


@task
def run_bets(bets: List[int]) -> None:
    if not bets:
        return None
    betting_records = query_markets_by_id_in_betting_record(bets)
    for record in betting_records:
        bet_record = BetRecord(*record)
        response = place_order(bet_record)
        if response.status == RESPONSE_SUCCESS:
            _update_successfully_places_bet(response, bet_record)
        else:
            update_betting_record_field(bet_record.id, "status", BettingRecordStatus.FAILED_TO_PLACE.name)
    return None


def _update_successfully_places_bet(response: PlaceOrders, bet_record: BetRecord):
    assert len(response.place_instruction_reports) == 1
    place_instruction = response.place_instruction_reports[0]
    bet_id = place_instruction.bet_id
    matched_stake = Decimal(place_instruction.size_matched)
    # TODO optimize call by updating field in a single call
    update_betting_record_field(bet_record.id, "bet_id", bet_id)
    update_betting_record_field(bet_record.id, "status", BettingRecordStatus.PLACED.name)
    update_betting_record_field(bet_record.id, "matched_stake", matched_stake)
