from typing import List

from airflow.operators.postgres_operator import PostgresOperator

from betfair_broker_pipeline.source.config import POSTGRES_CONNECTION_ID


def initialise_db_tables() -> PostgresOperator:
    return PostgresOperator(
        task_id="initialise_db_tables",
        sql = "sql/create_betting_record_table_and_indexes.sql",
        postgres_conn_id = POSTGRES_CONNECTION_ID,
    )
