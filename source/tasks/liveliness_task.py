from airflow.decorators import task

from betfair_broker_pipeline.source.constants import RESPONSE_SUCCESS
from betfair_broker_pipeline.source.utils.betfairlightweight_utils import get_logged_client, request_keep_alive


@task
def check_liveliness() -> None:
    api_client = get_logged_client()
    keep_alive = api_client.keep_alive()
    assert RESPONSE_SUCCESS == keep_alive.status
    return None
