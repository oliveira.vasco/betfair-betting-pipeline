from dataclasses import dataclass
from decimal import Decimal


@dataclass
class BetRecord:
    id: int
    market_id: str
    selection_id: str
    status: str
    intended_stake: Decimal
    matched_stake: Decimal
    side: str
    odd: Decimal
    order_type: str
    persistence_type: str
    bet_id: str
