from datetime import timedelta

BETFAIR_USERNAME = "<username>"
BETFAIR_PASSWORD = "<password>"
BETFAIR_APP_KEY = "<appkey>"

MARKET_FILTER_LIST_OF_EVENT_TYPE_IDS = []  # Add event type ids
MARKET_FILTER_LIST_OF_COUNTRIES = []  # Add event countries
MARKET_FILTER_TYPE_CODES = []  # Add type codes
MARKET_FILTER_TIME_DELTA_FROM_NOW = timedelta()  # Add time delta
MARKET_CATALOGUE_MAX_RESULTS = 1  # Add max number for market catalogue fetch

AWS_CONN_ID = "<aws-connection-id>"
AWS_BUCKET_ID = "<add-s3-bucket-id>"
AWS_S3_KEY_PREFIX = "<add-s3-key-prefix>"

POSTGRES_CONNECTION_ID = "<postgres-connection>"

MINIMUM_STAKE = 2  # 2 for EUR
