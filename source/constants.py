from enum import Enum

RESPONSE_SUCCESS = "SUCCESS"

class BettingRecordStatus(Enum):
    CREATED = 1
    PLACED = 2
    FAILED_TO_PLACE = 3
