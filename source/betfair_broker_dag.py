from airflow.decorators import dag

from datetime import datetime

from betfair_broker_pipeline.source.tasks.initialise_db_tables_task import initialise_db_tables
from betfair_broker_pipeline.source.tasks.betting_task_group import betting_task_group

default_args = {
    "start_date": datetime(2022, 1, 1)
}


@dag(schedule_interval="10,40 10-18 * * *", default_args=default_args, catchup=False)
def betfair_broker_dag():
    initialise_tables_task = initialise_db_tables()
    initialise_tables_task >> betting_task_group()

dag = betfair_broker_dag()
