CREATE TABLE IF NOT EXISTS betting_record (
    id SERIAL PRIMARY KEY,
    market_id VARCHAR(20) NOT NULL,
    selection_id VARCHAR(20) NOT NULL,
    status VARCHAR(20) DEFAULT 'CREATED' NOT NULL,
    intended_stake NUMERIC(10, 2) NOT NULL,
    matched_stake NUMERIC(10, 2) NOT NULL,
    side CHAR(4)  NOT NULL,
    odd NUMERIC(5, 2)  NOT NULL,
    order_type VARCHAR(10) NOT NULL,
    persistence_type VARCHAR(10) NOT NULL,
    bet_id VARCHAR(20) NULL
);
CREATE INDEX IF NOT EXISTS index_status ON betting_record (status);
CREATE INDEX IF NOT EXISTS index_market_id ON betting_record (market_id);
CREATE INDEX IF NOT EXISTS index_selection_id ON betting_record (selection_id);
CREATE INDEX IF NOT EXISTS index_matched_stake ON betting_record (matched_stake);
