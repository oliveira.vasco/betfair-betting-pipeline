from decimal import Decimal
import dataclasses
from typing import Dict, Union

from betfairlightweight import APIClient
from betfairlightweight.endpoints.keepalive import KeepAlive
from betfairlightweight.filters import cancel_instruction, limit_order, place_instruction, replace_instruction
from betfairlightweight.resources import PlaceOrders
from betfairlightweight.resources.authresources import KeepAliveResource

from betfair_broker_pipeline.source.config import BETFAIR_USERNAME, BETFAIR_PASSWORD, BETFAIR_APP_KEY, MINIMUM_STAKE
from betfair_broker_pipeline.source.models.bet_record import BetRecord


def get_logged_client() -> APIClient:
    client = APIClient(BETFAIR_USERNAME, BETFAIR_PASSWORD, app_key=BETFAIR_APP_KEY)
    client.login_interactive()
    return client

def request_keep_alive(client: APIClient) -> Union[dict, KeepAliveResource]:
    keep_alive = KeepAlive(client)
    return keep_alive()


def place_order(bet_record: BetRecord) -> PlaceOrders:
    betfair_client = get_logged_client()
    if bet_record.intended_stake >= MINIMUM_STAKE :
        response = betfair_client.betting.place_orders(
            market_id=bet_record.market_id, instructions=[_get_place_instruction_from_bet_record(bet_record)]
        )
    else:
        response = _place_order_under_minimum_stake(bet_record, betfair_client)
    return response


def _place_order_under_minimum_stake(bet_record: BetRecord, betfair_client: APIClient) -> PlaceOrders:
    initial_bet = _get_bet_record_under_minimum_stake(bet_record)
    inital_bet_response = betfair_client.betting.place_orders(
        market_id=initial_bet.market_id, instructions=[_get_place_instruction_from_bet_record(initial_bet)]
    )
    bet_id = inital_bet_response.place_instruction_reports[0].bet_id
    
    cancel_inst = {'betId': bet_id, 'sizeReduction': float(MINIMUM_STAKE - bet_record.intended_stake)}
    cancellation_response = betfair_client.betting.cancel_orders(market_id=initial_bet.market_id, instructions=[cancel_inst])

    replace_inst = {'betId': bet_id, 'newPrice': float(bet_record.odd)}
    replace_response = betfair_client.betting.replace_orders(market_id=initial_bet.market_id, instructions=[replace_inst])
    return inital_bet_response


def _get_bet_record_under_minimum_stake(bet_record: BetRecord) -> BetRecord:
    initial_bet = dataclasses.replace(bet_record)
    initial_bet.odd = _get_odd_for_initial_bet_under_minimum_stake(bet_record.side)
    initial_bet.intended_stake = MINIMUM_STAKE
    return initial_bet


def _get_odd_for_initial_bet_under_minimum_stake(side: str) -> Decimal:
    if side == "BACK":
        return Decimal(1000)
    elif side == "LAY":
        return Decimal(1.01)
    raise ValueError(f"Side has invalid value {side}")


def _get_place_instruction_from_bet_record(bet_record: BetRecord) -> Dict:
    return place_instruction(
        order_type=bet_record.order_type,
        selection_id=bet_record.selection_id,
        side=bet_record.side,
        limit_order=limit_order(
            price=float(bet_record.odd),
            persistence_type=bet_record.persistence_type,
            size=float(bet_record.intended_stake),
        )
    )
