from decimal import Decimal
from contextlib import contextmanager
from typing import Dict, List, Set, Union

from airflow.providers.postgres.hooks.postgres import PostgresHook

from betfair_broker_pipeline.source.config import POSTGRES_CONNECTION_ID

SELECT_RECORDS_QUERY = "SELECT market_id FROM betting_record WHERE market_id IN %s;"
SELECT_RECORDS_BY_ID_QUERY = "SELECT * FROM betting_record WHERE id IN %s;"
INSERT_RECORD_QUERY = """
    INSERT INTO betting_record 
    (market_id, selection_id, status, intended_stake, matched_stake, side, odd, order_type, persistence_type) 
    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)
    RETURNING id;
"""
UPDATE_RECORD_FIELD_BY_ID_QUERY = """UPDATE betting_record SET {} = %s WHERE id = %s;"""


@contextmanager
def postgres_cursor(with_commit: bool = False):
    postgres_hook = PostgresHook(postgres_conn_id=POSTGRES_CONNECTION_ID)
    connection = postgres_hook.get_conn()
    cursor = connection.cursor()
    try:
        yield cursor
        if with_commit:
            connection.commit()
    finally:
        cursor.close()
        connection.close()


def query_markets_in_betting_record(market_ids: List[str]) -> Set[str]:
    with postgres_cursor() as cursor:
        cursor.execute(SELECT_RECORDS_QUERY, (tuple(market_ids),))
        betting_records = cursor.fetchall()
    return set(map(lambda m: m[0], betting_records))


def query_markets_by_id_in_betting_record(record_ids: List[str]):
    with postgres_cursor() as cursor:
        cursor.execute(SELECT_RECORDS_BY_ID_QUERY, (tuple(record_ids),))
        betting_records = cursor.fetchall()
    return betting_records


def insert_betting_record(
    market_id: str,
    selection_id: str,
    intended_stake: Decimal,
    matched_stake: Decimal,
    side: str,
    odd: Decimal,
    order_type: str,
    persistence_type: str,
    status: str,
) -> Dict:
    with postgres_cursor(with_commit=True) as cursor:
        cursor.execute(
            INSERT_RECORD_QUERY, 
            (market_id, selection_id, status, intended_stake, matched_stake, side, odd, order_type, persistence_type),
        )
        row_id = cursor.fetchone()[0]
    return row_id


def update_betting_record_field(id: int, field_name: str, value: Union[str, Decimal, int]):
    with postgres_cursor(with_commit=True) as cursor:
        cursor.execute(
            UPDATE_RECORD_FIELD_BY_ID_QUERY.format(field_name), (value, id)
        )
    return None
